%% Stationary file for preparing manuscripts for journal submission. This document uses the Elsevier article class and was customized by Alberto Soto (Summer 2018). 

\documentclass[titlepage,11pt]{article}

%% Standard Packages
\usepackage{graphics,graphicx,epsfig,epsf,mathrsfs, epstopdf}
\usepackage{amsmath,amsthm,amssymb}

% Natbib style for bibliography
\usepackage{natbib}
\def\bibfont{\footnotesize} 				% smaller font for bibliography 

%% Line numbers
\usepackage{lineno}		
\modulolinenumbers[2]				% Set line number step

%% Other packages
\usepackage{siunitx}					% For special SI units
\usepackage[]{units}					% For nice formatting units
\usepackage[ragged]{sidecap}			% For adding side captions to figure (instead of under)
\usepackage{enumitem} 				% For adding items in a multiple choice list
\usepackage{tasks} 					% Adds multiple choice items in one single line 
\usepackage{tabu} 					% For creating tables 
\usepackage{caption}				% Better control of figure captions

\captionsetup{margin=10pt,font=small,labelfont=bf,listformat=simple}		% Caption options
\usepackage{subcaption}										% Subcaptions for multiple figs in a float
\usepackage[nomarkers,figuresonly]{endfloat}						% All figures at end of pdf
\renewcommand{\efloatseparator}{\mbox{}}

%% Custom definitions
\def\ds{\displaystyle}
\def\d{\partial}
\def\mat{MATLAB }

%% New commands
\newcommand{\rr}{\mathbb{R}}
\newcommand{\zz}{\mathbb{Z}}
\newcommand{\nn}{\mathbb{N}}
\newcommand{\ee}{\mathscr{E}}
\newcommand{\mb}{\mathbf}
\newcommand{\ol}{\overline}


\begin{document}

%% Title
\title{The hydrodynamics and locomotor strategies of predator--prey interactions in zebrafish}

%% author
\author{ \textbf{Alberto Soto} \and Department of Ecology and Evolutionary Biology \and University of California, Irvine \\ \\
Dissertation Prospectus}

\maketitle

%%
%% Start line numbering here if you want
%%
%\linenumbers


\section*{Introduction}
\label{S:intro}
Organisms need to eat and avoid being eaten to survive. 
These two challenges have major implications for the behavioral and locomotor strategies they exhibit.
Many fishes are subject to predation at all life stages and are predators themselves. 
Zebrafish are a small freshwater species (Family Cyprinidae) of fish native to rivers and inland streams of India {\citep{Parichy:2015hh}. 
They are omnivores and begin actively hunting for food at 5 days post fertilization (dpf). 
In the wild, they also have to contend with predation by other fish such as the knifefish (\textit{Notopterus)} \citep{Parichy:2015hh}.
In the lab, juvenile and adult zebrafish will readily prey on larvae of the same species.
This makes it tractable to study their behavior and locomotion as predator and prey at the same time, depending on the fish's life stage.
In my dissertation research, I aim to address the biomechanics of active pursuit in zebrafish and the escape strategy of prey fish. 
This work will address the locomotor strategies fish implement to pursue prey and evade predators.

\section*{Dissertation Research}
\label{S:research}

%%\subsection{Chapter 1: Locomotion and control of zebrafish chasing prey}
 \subsection*{Chapter 1: How do zebrafish predators approach their prey and control their locomotion?}
 
\input{chapters/Chapter_1.tex}

%%\subsection{Chapter 2: The biomechanics of zebrafish turning maneuvers} 
\subsection*{Chapter 2: How do zebrafish execute turning maneuvers?} 

%\input{chapters/Chapter_2.tex}
   
  
%%\subsection{Chapter 3: A mathematical and experimental investigation of optimal escape strategy.}
\subsection*{Chapter 3: Do prey fish evade predators with an optimal escape strategy?}

% \input{chapters/Chapter_3.tex}


%\bibliographystyle{jxb}
%\bibliography{Soto-Dissertation-Prospectus}

\newpage 


\begin{figure}[ht]
\centering
\includegraphics[width=1.0\textwidth]{./figures/ch1-summary-fig.pdf}
\caption[The kinematics of pursuit in juvenile zebrafish]
{The kinematics of pursuit in juvenile zebrafish. 
(A) The predator's heading angle is defined relative to the horizontal and the bearing angle is defined with respect to heading.
 Here the bearing angle is positive. 
 (B) Sample time series of distance between predator and prey (purple), bearing angle (green), and heading angle (blue). 
 The gray bars highlight the burst phase of burst--and--coast swimming where the predator accelerates and changes heading. 
 (C) The change in heading angle is correlated to the bearing angle before the turn is initiated. 
 (D) Representative traces of heading angle and lateral tail excursion through time for two turning maneuvers (highlighted with vertical bars).}
\label{fig:ch1}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=1.0\textwidth]{./figures/ch2-summary-figv2.pdf}
\caption[PIV analysis of zebrafish turning maneuvers]
{PIV analysis of zebrafish turning maneuvers. 
(A) Photograph of experimental setup showing the arrangement of swim tank, high--speed camera, laser, optics, and IR panel. 
(B) Schematic of syncing method for simultaneous acquisition of images for PIV and automatic tracking using a single camera. 
A 2--channel arbitrary function generator delivers square wave signals to the high--speed camera, laser and IR panel. 
The camera's frame rate is set by the frequency of the square wave, here $f_{\text{cam}} = \unit[1000]{Hz}$, and the shutter speed is set to $1/f_{\text{cam}}$. 
The laser and IR panel receive a square wave signal with frequency $\frac{1}{2}f_{\text{cam}}$. 
The laser emits during the peak of the square wave only, while the IR panel is designed to emit during the trough of the same signal. 
(C) Sample images of a zebrafish performing a turning maneuver captured with this method. 
(D) Instantaneous velocity field (left panel) and vorticity (right panel) of zebrafish as it completes a turn.}
\label{fig:ch2}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=1.0\textwidth]{./figures/ch3-summary-figv2.pdf}
\caption[When optimal strategy matters to prey fish]
{A pursuit--evasion model for piscivorous interactions. 
(A) The model considers a predator traveling with speed $U$ toward a prey with speed $V$ and escape angle $\alpha$. 
The general model also considers $\theta$, the lateral position of the prey relative to the predator's approach. 
(B) Numerical solutions to the equation for minimum distance $\overline D_{\text{min}}$ as a function of escape angle ($\alpha$) and relative speed ($K=U/V$). 
The black curves are the optimal solutions found by Weihs and Webb (1984).
The performance plateau when $K<1$ indicates that differences in $\alpha$ have no effect on $\overline D_{\text{min}}$. 
The effect of deviation from the optimum for $K>1$ is shown by the boundaries of a decrease of $0.1\overline D_{\text{min}}$ (gray curves). 
(C) The model predictions were compared with the escape responses (small arrows) of larval zebrafish that were approached by a robotic predator \citep{Stewart:2014cm}. (D) Numerical results of the simulated interactions show how the minimum distance varies with the escape angle angle ($\alpha$) and the lateral displacement ($\theta$). Performance plateaus (in yellow) decrease in area as relative predator speed increases. (D) Zebrafish larvae escape angles $\alpha$ generally lie within the predicted performance plateaus. }
\label{fig:ch3}
\end{figure}

% Sample text for inserting figures
%\begin{figure}[h]
%\centering\includegraphics[width=0.4\linewidth]{placeholder}
%\caption{Figure caption}
%\end{figure}

\end{document}